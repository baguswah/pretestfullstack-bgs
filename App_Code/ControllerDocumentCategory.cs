﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerCategory
/// </summary>
public class ControllerDocumentCategory : ClassBase
{
    public ControllerDocumentCategory(DataClassesDatabaseDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public TBDocumentCategory[] Data()
    {
        return db.TBDocumentCategories.ToArray();
    }

    //Create data
    public TBDocumentCategory Create(string name)
    {
        TBDocumentCategory documentcategory = new TBDocumentCategory
        {
            Name = name,
            UID = Guid.NewGuid(),
            CreatedBy = 1,
            CreatedAt = DateTime.Now
        };

        db.TBDocumentCategories.InsertOnSubmit(documentcategory);

        return documentcategory;
    }

    //Search data
    public TBDocumentCategory Cari(string UID)
    {
        return db.TBDocumentCategories.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    //Update
    public TBDocumentCategory Update(string UID, string name)
    {
        var data = Cari(UID);

        if (data != null)
        {
            data.Name = name;

            return data;
        }
        else
            return null;
    }

    //Delete
    public TBDocumentCategory Delete(string UID)
    {
        var data = Cari(UID);

        if (data != null)
        {
            db.TBDocumentCategories.DeleteOnSubmit(data);
            db.SubmitChanges();

            return data;
        }
        else
            return null;
    }

    public void DropDownListCategory(DropDownList dropDownList)
    {
        dropDownList.DataSource = Data();
        dropDownList.DataValueField = "ID";
        dropDownList.DataTextField = "Name";
        dropDownList.DataBind();

        dropDownList.Items.Insert(0, new ListItem { Value = "0", Text = "-Pilih-" });
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> listItems = new List<ListItem>();

        listItems.Add(new ListItem { Value = "0", Text = "-Pilih-" });

        listItems.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));

        return listItems.ToArray();
    }
}