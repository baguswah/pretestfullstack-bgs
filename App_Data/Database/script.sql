USE [master]
GO
/****** Object:  Database [PretestBagusWahyu]    Script Date: 8/4/2023 10:11:22 AM ******/
CREATE DATABASE [PretestBagusWahyu]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PretestBagusWahyu', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\PretestBagusWahyu.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PretestBagusWahyu_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\PretestBagusWahyu_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [PretestBagusWahyu] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PretestBagusWahyu].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PretestBagusWahyu] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ARITHABORT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PretestBagusWahyu] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PretestBagusWahyu] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PretestBagusWahyu] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PretestBagusWahyu] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PretestBagusWahyu] SET  MULTI_USER 
GO
ALTER DATABASE [PretestBagusWahyu] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PretestBagusWahyu] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PretestBagusWahyu] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PretestBagusWahyu] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PretestBagusWahyu] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PretestBagusWahyu] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [PretestBagusWahyu] SET QUERY_STORE = ON
GO
ALTER DATABASE [PretestBagusWahyu] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [PretestBagusWahyu]
GO
/****** Object:  Table [dbo].[TBCompany]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBCompany](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBCompany] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBDocumentCategory]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBDocumentCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBDocumentCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBDocument]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBDocument](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDCategory] [int] NULL,
	[Name] [varchar](255) NULL,
	[Description] [text] NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBDocument] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_Document]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Document]
AS
SELECT dbo.TBDocument.IDCompany, dbo.TBDocument.IDCategory, dbo.TBDocument.Name, dbo.TBDocument.Description, dbo.TBDocument.Flag, dbo.TBDocument.CreatedBy, dbo.TBDocument.CreatedAt, 
                  dbo.TBDocumentCategory.Name AS Expr1, dbo.TBCompany.Name AS Expr2
FROM     dbo.TBDocument INNER JOIN
                  dbo.TBDocumentCategory ON dbo.TBDocument.IDCategory = dbo.TBDocumentCategory.ID INNER JOIN
                  dbo.TBCompany ON dbo.TBDocument.IDCompany = dbo.TBCompany.ID
GO
/****** Object:  Table [dbo].[TBUser]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDPosition] [int] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](255) NULL,
	[Role] [varchar](50) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBPosition]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBPosition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBPosition] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_User]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_User]
AS
SELECT dbo.TBUser.IDCompany, dbo.TBUser.IDPosition, dbo.TBUser.Address, dbo.TBUser.Name, dbo.TBUser.Email, dbo.TBUser.Telephone, dbo.TBUser.Username, dbo.TBUser.Password, dbo.TBUser.Role, dbo.TBUser.Flag, 
                  dbo.TBUser.CreatedBy, dbo.TBUser.CreatedAt, dbo.TBCompany.Name AS Expr1, dbo.TBPosition.Name AS Expr2
FROM     dbo.TBCompany INNER JOIN
                  dbo.TBDocument ON dbo.TBCompany.ID = dbo.TBDocument.IDCompany INNER JOIN
                  dbo.TBDocumentCategory ON dbo.TBDocument.IDCategory = dbo.TBDocumentCategory.ID INNER JOIN
                  dbo.TBPosition ON dbo.TBCompany.ID = dbo.TBPosition.ID INNER JOIN
                  dbo.TBUser ON dbo.TBCompany.ID = dbo.TBUser.IDCompany AND dbo.TBPosition.ID = dbo.TBUser.IDPosition
GO
ALTER TABLE [dbo].[TBDocument]  WITH CHECK ADD  CONSTRAINT [FK_TBDocument_TBCompany] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[TBCompany] ([ID])
GO
ALTER TABLE [dbo].[TBDocument] CHECK CONSTRAINT [FK_TBDocument_TBCompany]
GO
ALTER TABLE [dbo].[TBDocument]  WITH CHECK ADD  CONSTRAINT [FK_TBDocument_TBDocumentCategory1] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[TBDocumentCategory] ([ID])
GO
ALTER TABLE [dbo].[TBDocument] CHECK CONSTRAINT [FK_TBDocument_TBDocumentCategory1]
GO
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TBCompany] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[TBCompany] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TBCompany]
GO
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TBPosition] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[TBPosition] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TBPosition]
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateCompany]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateCompany]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

INSERT INTO [dbo].[TBCompany]
           ([UID]
           ,[Name]
           ,[Address]
		   ,[Email]
		   ,[Telephone]
		   ,[Flag]
		   ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
			,@name
			,@address
			,@email
			,@telephone
			,@flag
			,@createdby
           ,GETDATE())

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocument]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateDocument]
    -- Add the parameters for the stored procedure here
    @idcompany int,
    @idcategory int,
    @name varchar(255),
    @description text, 
    @flag int,
    @createdby int,
    @retVal int OUTPUT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    -- Insert statements for procedure here
    INSERT INTO [dbo].[TBDocument]
           ([UID]
           ,[IDCompany]
           ,[IDCategory]
           ,[Name]
           ,[Description]
           ,[Flag]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
           ,@idcompany
           ,@idcategory
           ,@name
           ,@description
           ,@flag
           ,@createdby
           ,GETDATE())

    IF(@@ROWCOUNT > 0)
    BEGIN
        SET @retVal = 200
    END
    ELSE
    BEGIN
        SET @retVal = 500
    END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocumentCategory]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateDocumentCategory]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

INSERT INTO [dbo].[TBDocumentCategory]
           ([UID]
           ,[Name]
		   ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
           ,@name
		   ,@createdby
           ,GETDATE())

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreatePosition]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreatePosition]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

INSERT INTO [dbo].[TBPosition]
    (    [UID],
		[Name],
		[CreatedBy],
		[CreatedAt]
 )
 VALUES
	(
		NEWID(),
		@name,
		@createdby,
		GETDATE()
	)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateUser]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateUser] 
	-- Add the parameters for the stored procedure here
	@idcompany int,
	@idposition int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [PretestBagusWahyu].[dbo].[TBUser]
         ([UID]
           ,[IDCompany]
           ,[IDPosition]
           ,[Name]
           ,[Address]
           ,[Email]
           ,[Telephone]
           ,[Username]
           ,[Password]
           ,[Role]
           ,[Flag]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID(),
		   @idcompany,
           @idposition,
           @name,
           @address,
           @email,
           @telephone,
           @username,
           CONVERT(VARCHAR(32),HashBytes('MD5',@password),2),
           @role,
           @flag,
           @createdby,
           GETDATE())
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteCompany]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBCompany]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocument]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteDocument]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBDocument]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocumentCategory]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteDocumentCategory]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBDocumentCategory]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeletePosition]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeletePosition]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBPosition]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUser]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteUser]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DELETE 
	FROM
	[dbo].[TBUser]

	WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_loginUser]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_loginUser]
	@email varchar(50),
	@password varchar(50),
	@retVal int OUTPUT

AS
BEGIN
SET NOCOUNT ON;
	SELECT
		[ID],
		[Username],
		[Email],
		[Role],
		[CreatedAt]
	FROM TBUser
	WHERE 
		[Email] = @email and [Password] = CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2)

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateCompany]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statements for procedure here

UPDATE [dbo].[TBCompany] SET
           [Name]=@name
           ,[Address]=@address
           ,[Email]=@email
           ,[Telephone]=@telephone
           ,[Flag]=@flag
           ,[CreatedBy]= @createdby

  WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocument]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateDocument]
	-- Add the parameters for the stored procedure here
	@id int,
	@idcompany int,
    @idcategory int,
    @name varchar(255),
    @description text,
    @flag int,
    @createdby int,
    @retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

UPDATE [dbo].[TBDocument] SET
           [IDCompany] = @idcompany
           ,[IDCategory] = @idcategory
           ,[Name] = @name
           ,[Description] = @description
           ,[Flag] = @flag
		   ,[CreatedBy] = @createdby

  WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocumentCategory]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateDocumentCategory]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(255),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

UPDATE [dbo].[TBDocumentCategory] SET
           [Name] = @name
		   ,[CreatedBy] = @createdby

		   WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
    
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdatePosition]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdatePosition]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

UPDATE [dbo].[TBPosition] SET
           [Name] = @name
		   ,[CreatedBy] = @createdby

		   WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
    
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUser]    Script Date: 8/4/2023 10:11:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUser]
	-- Add the parameters for the stored procedure here
	@id int,
	@idcompany int,
	@idposition int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(255),
	@role varchar (50),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

UPDATE [dbo].[TBUser] SET
           [IDCompany] = @idcompany
		   ,[IDPosition] = @idposition
		   ,[Name] = @name
           ,[Address] = @address
		   ,[Email] = @email
		   ,[Telephone] = @telephone
		   ,[Username] = @username
		   ,[Password] = CONVERT(varchar(32), HASHBYTES('MD5', @password), 2)
		   ,[Role] = @role
		   ,[Flag] = @flag
		   ,[CreatedBy] = @createdby

  WHERE ID = @id

	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -120
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBDocument"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBDocumentCategory"
            Begin Extent = 
               Top = 139
               Left = 314
               Bottom = 302
               Right = 508
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBCompany"
            Begin Extent = 
               Top = 8
               Left = 565
               Bottom = 171
               Right = 759
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2988
         Alias = 900
         Table = 2256
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBCompany"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 170
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBDocument"
            Begin Extent = 
               Top = 161
               Left = 265
               Bottom = 324
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBDocumentCategory"
            Begin Extent = 
               Top = 175
               Left = 480
               Bottom = 338
               Right = 674
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBPosition"
            Begin Extent = 
               Top = 150
               Left = 870
               Bottom = 312
               Right = 1064
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBUser"
            Begin Extent = 
               Top = 0
               Left = 633
               Bottom = 163
               Right = 827
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 135' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'0
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
USE [master]
GO
ALTER DATABASE [PretestBagusWahyu] SET  READ_WRITE 
GO
