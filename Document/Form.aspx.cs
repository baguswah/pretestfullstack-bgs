﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropdown();
            LoadCategory();
            FormUpdate();
        }
    }

    public void LoadDropdown()
    {

        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            ControllerCompany controllerCompany = new ControllerCompany(db);
            ListItem[] companyItems = controllerCompany.DropDownList();
            ListCompany.Items.AddRange(companyItems);

        }
    }

    public void LoadCategory()
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            ControllerDocumentCategory category = new ControllerDocumentCategory(db);
            ListItem[] categoryItems = category.DropDownList();
            ListCategory.Items.AddRange(categoryItems);
        }
    }

    public void FormUpdate()
    {


        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            ControllerDocument controllerDocument = new ControllerDocument(db);
            var document = controllerDocument.Cari(Convert.ToInt32(Request.QueryString["id"]));
            if (document != null)
            {

                ListCompany.SelectedValue = document.IDCompany.ToString();
                ListCategory.SelectedValue = document.IDCategory.ToString();
                InputName.Text = document.Name;
                InputDescription.Text = document.Description;


                ButtonOk.Text = "Edit";
                LabelTitle.Text = "Edit Document";
            }
            else
            {
                ButtonOk.Text = "Add New";
                LabelTitle.Text = "Add New Document";
            }
        }

    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerDocument controllerDocument = new ControllerDocument(db);

                if (ButtonOk.Text == "Add New")
                {
                    controllerDocument.Create(int.Parse(ListCompany.SelectedValue), int.Parse(ListCategory.SelectedValue), InputName.Text, InputDescription.Text);
                }
                else if (ButtonOk.Text == "Edit")
                {
                    controllerDocument.Update(int.Parse(Request.QueryString["id"]), int.Parse(ListCompany.SelectedValue), int.Parse(ListCategory.SelectedValue), InputName.Text, InputDescription.Text);

                }
                db.SubmitChanges();
                Response.Redirect("/Document/Default.aspx");
            }
        }
    }
}