﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            repeaterDocument.DataSource = db.TBDocuments.Select(x => new
            {
                x.ID,
                x.UID,
                x.Name,
                NameCompany = x.TBCompany.Name,
                NameCategory = x.TBDocumentCategory.Name,
                x.Description
            }).ToArray();
            repeaterDocument.DataBind();
        }

    }

    protected void repeaterDocument_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Document/Form.aspx?id=" + e.CommandName.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var document = db.TBDocuments.FirstOrDefault(x => x.ID.ToString() == e.CommandArgument.ToString());

                db.TBDocuments.DeleteOnSubmit(document);
                db.SubmitChanges();

                LoadData();

            }
        }
    }


}