﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }

        }
    }

    //Menampilkan Data

    protected void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerCompany controllerCompany = new ControllerCompany(db);

        repeaterCompany.DataSource = controllerCompany.Data();
        repeaterCompany.DataBind();
    }




    //FUngsi untuk update dan delete dalam table repeater
    protected void repeaterCompany_itemCommand(object source, RepeaterCommandEventArgs e) 
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Company/Form.aspx?uid=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var company = db.TBCompanies.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                db.TBCompanies.DeleteOnSubmit(company);
                db.SubmitChanges();

                LoadData(db);


            }
        }
    }
}