﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }
        }
    }
    private void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerUser controllerUser = new ControllerUser(db);
        repeaterUser.DataSource = db.TBUsers
        .Select(x => new
        {
            x.ID,
            x.IDCompany,
            NameCompany = x.TBCompany.Name,
            x.IDPosition,
            NamePosition = x.TBPosition.Name,
            x.Name,
            x.Address,
            x.Email,
            x.Telephone,
            x.Username,
            x.Role,
            x.Flag,
            x.CreatedBy

        }).ToArray();
        repeaterUser.DataBind();
    }
    protected void repeaterUser_itemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/User/Forms.aspx?ID=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var user = db.TBUsers.FirstOrDefault(x => x.ID.ToString() == e.CommandArgument.ToString());
                db.TBUsers.DeleteOnSubmit(user);
                db.SubmitChanges();
                LoadData(db);
            }
        }
    }
}