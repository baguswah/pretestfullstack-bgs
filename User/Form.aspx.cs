﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.PeerToPeer;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);
                ControllerPosition controllerPosition = new ControllerPosition(db);
                ListCompany.Items.AddRange(controllerCompany.DropDownList());
                ListPosition.Items.AddRange(controllerPosition.DropDownList());
                if (Request.QueryString["ID"] != null)
                {
                    ControllerUser controllerUser = new ControllerUser(db);
                    var user = controllerUser.Cari(int.Parse(Request.QueryString["ID"]));
                    if (user != null)
                    {
                        ListCompany.SelectedValue = user.IDCompany.ToString();
                        ListPosition.SelectedValue = user.IDPosition.ToString();
                        InputName.Text = user.Name;
                        InputAddress.Text = user.Address;
                        InputEmail.Text = user.Email;
                        InputTelephone.Text = user.Telephone;
                        InputUser.Text = user.Username;
                        InputRole.Text = user.Role;
                        InputFlag.Text = user.Flag.ToString();
                        InputCreatedBy.Text = user.CreatedBy.ToString();

                        ButtonOk.Text = "Update";
                        LabelTitle.InnerText = "Update";
                    }
                }
                else
                {
                    ButtonOk.Text = "Add New";
                    LabelTitle.InnerText = "Add New";
                }
            }
        }
    }
    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                ControllerUser controllerUser = new ControllerUser(db);
                if (ButtonOk.Text == "Add New")
                {
                    controllerUser.Create(
                        int.Parse(ListCompany.SelectedValue),
                        int.Parse(ListPosition.SelectedValue),
                        InputName.Text,
                        InputAddress.Text,
                        InputEmail.Text,
                        InputTelephone.Text,
                        InputUser.Text,
                        InputPassword.Text,
                        InputRole.Text,
                        int.Parse(InputFlag.Text),
                        int.Parse(InputCreatedBy.Text)
                        );
                }
                else if (ButtonOk.Text == "Update")
                {
                    controllerUser.Update(
                        int.Parse(Request.QueryString["ID"]),
                        int.Parse(ListCompany.SelectedValue),
                        int.Parse(ListPosition.SelectedValue),
                        InputName.Text,
                        InputAddress.Text,
                        InputEmail.Text,
                        InputTelephone.Text,
                        InputUser.Text,
                        InputPassword.Text,
                        InputRole.Text,
                        int.Parse(InputFlag.Text),
                        int.Parse(InputCreatedBy.Text)
                        );
                }
                db.SubmitChanges();
                Response.Redirect("/User/Default.aspx");
            }
        }
    }
}
