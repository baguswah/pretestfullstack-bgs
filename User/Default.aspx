﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="User_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
    List User
    <hr />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <a href="Form.aspx" class="btn btn-success btn-sm" id="button-add">Add User</a>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Company</th>
                                <th>Position</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Username</th>
                                <th>Role</th>
                               
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="repeaterUser" Runat="server" OnItemCommand="repeaterUser_itemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td class="fitSize"><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("NameCompany") %></td>
                                        <td><%# Eval("NamePosition") %></td>
                                        <td><%# Eval("Name") %></td>
                                        <td><%# Eval("Address ") %></td>
                                        <td><%# Eval("Email") %></td>
                                        <td><%# Eval("Telephone") %></td>
                                        <td><%# Eval("Username") %></td>
                                        <td><%# Eval("Role") %></td>
                                        <td>
                                            <asp:Button ID="btnUpdate" runat="server" CommandName="Update" CssClass="btn btn-info btn-sm" Text="Update" CommandArgument='<%# Eval("ID") %>'/>
                                            <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btn btn-danger btn-sm" Text="Delete" CommandArgument='<%# Eval("ID") %>'/>

                                        </td>
                                </tr>
                            </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" Runat="Server">
</asp:Content>
