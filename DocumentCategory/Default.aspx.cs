﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentCategory_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
            {
                LoadData(db);
            }
        }
    }
    private void LoadData(DataClassesDatabaseDataContext db)
    {
        ControllerDocumentCategory controllerDocumentCategory = new ControllerDocumentCategory(db);
        repeaterDocumentCategory.DataSource = controllerDocumentCategory.Data();
        repeaterDocumentCategory.DataBind();
    }
    protected void repeaterDocumentCategory_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesDatabaseDataContext db = new DataClassesDatabaseDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/DocumentCategory/Forms.aspx?uid=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var documentCatergory = db.TBDocumentCategories.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());
                db.TBDocumentCategories.DeleteOnSubmit(documentCatergory);
                db.SubmitChanges();
                LoadData(db);
            }
        }
    }
}